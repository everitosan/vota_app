from flask_restful import Api
from api import Votants, Votant, verify, Video, Videos, get_approved
from flask import render_template, request
import json

def register(app):
  @app.route('/')
  def home(name=None):
    return render_template('index.html', name=name)

  @app.route('/video/appoved', methods=['GET'])
  def res(name=None):
    return json.dumps(get_approved())

  api = Api(app)
  api.add_resource(Votants, '/votant')
  api.add_resource(Votant, '/votant/<string:id>')
  api.add_resource(Videos, '/video')
  api.add_resource(Video, '/video/<string:id>')