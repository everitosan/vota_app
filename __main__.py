import os
from flask import Flask
from sql_db import SQL_DB
from flask_cors import CORS
from dotenv import load_dotenv
load_dotenv()


app = Flask(__name__)
db = SQL_DB.get_db(app)
cors = CORS(app, resources={r"/*": {"origins": os.getenv('ORIGIN')}})

import routes
routes.register(app)

if __name__ == '__main__':
  app.run(debug=True)
