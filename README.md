# Vota x Nosotros backend app

The app it has been developed in python 3. To install the dependencies you may use a virtual environment.


```sh
$ virtualenv -p python3 env
```

Once created, activate it and install the dependencies.

```sh
$ pip install -r requirements.txt
```

Then create the database.

```sh
$ python migrate.py
```

To start the server, you could use upstart service. First copy the vota_app.conf file to /etc/init/ directory.

```
$ cp vota_app.conf /etc/init/
```

Start the service.

```
$ service vota_app start
```

