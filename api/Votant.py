from flask_restful import Resource, reqparse
from models import Votant as VotantModel, VotantSchema
from api.Base import RestCrudDetail, RestCrud
from requests import post
import os

parser = reqparse.RequestParser()
parser.add_argument('code', type=str, help='Code of instagram', required=True)
parser.add_argument('country', type=str, help='Country of votant', required=True)


def verify(code):
  valid = None
  UID = None

  data = {
    "client_id": os.getenv("CLIENT_ID"),
    "client_secret": os.getenv("CLIENT_SECRET"),
    "grant_type": "authorization_code",
    "redirect_uri": os.getenv("REDIRECT_URI"),
    "code": code,
  }
  res = post("https://api.instagram.com/oauth/access_token", data=data)
  user_data = res.json()

  try:
    UID = user_data['user']['id']
    valid = True
  except KeyError as e:
    valid = False
    UID = user_data

  return valid, UID

class Votants(RestCrud):
  model = VotantModel
  schema = VotantSchema
  parser = parser

  def post(self):
    args = parser.parse_args()
    is_valid, UID = verify(args['code'])
    
    if is_valid:
      record = self.model.query.filter_by(UID=UID).first()
      schema = self.schema()

      if record is None:
        record = self.model(UID=UID, country=args['country'])

        self.db.session.add(record)
        self.db.session.commit()

      return schema.dump(record).data
    else: 
      return UID

class Votant(RestCrudDetail):
  model = VotantModel
  schema = VotantSchema
  parser = parser
