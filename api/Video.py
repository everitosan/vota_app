from flask_restful import Resource, reqparse
from models import Video as VideoModel, VideoSchema
from api.Base import RestCrudDetail, RestCrud

parser = reqparse.RequestParser()
parser.add_argument('url', type=str, help='Url of video', required=True)
parser.add_argument('approved', type=int, help='approved status of video')


parser_patch = reqparse.RequestParser()
parser_patch.add_argument('url', type=str, help='Url of video')
parser_patch.add_argument('approved', type=int, help='approved status of video')


class Videos(RestCrud):
  model = VideoModel
  schema = VideoSchema
  parser = parser

class Video(RestCrudDetail):
  model = VideoModel
  schema = VideoSchema
  parser = parser_patch

def get_approved():
  videos = VideoModel.query.filter_by(approved=1)
  schema = VideoSchema(many=True)

  return schema.dump(videos).data

 