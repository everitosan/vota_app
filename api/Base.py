from flask_restful import Resource, reqparse
from sql_db import SQL_DB


class BaseRestCrud(Resource):
  def __init__(self, *args, **kwargs):
    self.db = SQL_DB.get_db()
    self.model = self.model
    self.parser = self.parser
    self.schema = self.schema

class RestCrudDetail(BaseRestCrud):

  def get(self, id):
    register = self.model.query.get(id)
    translate = self.schema()
    return translate.dump(register).data

  def patch(self, id):
    record = self.model.query.get(id)
    args = self.parser.parse_args()

    for key, val in args.items():
      if val is not None:
        setattr(record, key, val)

    self.db.session.commit()

    schema = self.schema()
    return schema.dump(record).data


class RestCrud(BaseRestCrud):

  def post(self):
    record = self.model()
    args = self.parser.parse_args()

    for key, val in args.items():
      if val is not None:
        setattr(record, key, val)

    self.db.session.add(record)
    self.db.session.commit()

    schema = self.schema()
    return schema.dump(record).data

  def get(self):
    records = self.model.query.all()
    schema = self.schema(many=True)

    return schema.dump(records).data



