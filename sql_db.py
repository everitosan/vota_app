from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


class SQL_DB():
  __db_instance = None
  __ma_instance = None

  @staticmethod
  def get_db(app=None):
    if SQL_DB.__db_instance == None:
      SQL_DB(app)
    return SQL_DB.__db_instance

  @staticmethod
  def get_marshmallow():
    if SQL_DB.__ma_instance == None:
      raise Exception("Cannot use Marshmallow before instantiating SQLAlchemy")
    return SQL_DB.__ma_instance

  def __init__(self, app):
    if SQL_DB.__db_instance == None:
      if app is None:
        app = Flask(__name__)
      app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./sqlite3.db'
      app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
      SQL_DB.__db_instance = SQLAlchemy(app)
      SQL_DB.__ma_instance = Marshmallow(app)

