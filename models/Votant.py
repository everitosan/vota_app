from flask_sqlalchemy import SQLAlchemy
from sql_db import SQL_DB

db = SQL_DB.get_db()
ma = SQL_DB.get_marshmallow()


class Votant(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  UID = db.Column(db.String(80), unique=True, nullable=False)
  country = db.Column(db.String(120), unique=False, nullable=False)

  def __repr__(self):
    return "{} {} {}".format(self.id, self.UID, self.country)

class VotantSchema(ma.ModelSchema):
    class Meta:
      model = Votant