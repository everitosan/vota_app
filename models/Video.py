from flask_sqlalchemy import SQLAlchemy
from sql_db import SQL_DB

db = SQL_DB.get_db()
ma = SQL_DB.get_marshmallow()

class Video(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  approved = db.Column(db.Integer, default=0, nullable=False)
  url = db.Column(db.String(255), nullable=False)

  def __repr__(self):
    return "{} {} {}".format(self.id, self.approved, self.url)

class VideoSchema(ma.ModelSchema):
  class Meta:
    model = Video